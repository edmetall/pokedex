import {useEffect,useState} from 'react'
import axios from 'axios'
import Paginacion from './Paginacion'
import Pokemon from './Pokemon'

const URL = 'https://pokeapi.co/api/v2/pokemon/'

export default function Listado() {
    const [pokemons, setPokemons] = useState()
    const [paginaAnterior, setPaginaAnterior] = useState()
    const [paginaSiguiente, setPaginaSiguiente] = useState()
    const [paginaActual, setPaginaActual] = useState(URL)
    const [cargando, setCargando] = useState(true)

    useEffect(() => {
        const getPokemons = async() => {
            setCargando(true)
            try {
                const res = await axios.get(paginaActual)
                const data = res.data
                setPokemons(data.results)
                setPaginaAnterior(data.previous)
                setPaginaSiguiente(data.next)
                setCargando(false)
            } catch (error) {
                console.log(error)
                setCargando(false)
            }
            

        }
        getPokemons()
    }, [paginaActual])

    const irPaginaSiguiente = () => {
        setPaginaActual(paginaSiguiente)
    }
    const irPaginaAnterior = () => {
        setPaginaActual(paginaAnterior)
    }

    if (cargando) return "Cargando informacion..."

    return (
        <div>
            <Paginacion 
                irPaginaAnterior={paginaAnterior ? irPaginaAnterior : null}
                irPaginaSiguiente={paginaSiguiente ? irPaginaSiguiente : null}
            />
            <div className='card-group m-4'>
            {
                pokemons && pokemons.map(pokemon => {
                    console.log(pokemon.name)
                    return <div className='col-lg-3 col-md-4 my-4'><Pokemon param={pokemon.name} /></div>
                })
            }
            </div>
            <Paginacion 
                irPaginaAnterior={paginaAnterior ? irPaginaAnterior : null}
                irPaginaSiguiente={paginaSiguiente ? irPaginaSiguiente : null}
            />
        </div>
    )
}
