import {useState} from 'react'

export default function Navbar() {
    const [input, setInput] = useState()

    const handleChange = (e) => {
        setInput(e.target.value)
    }

    const handleSubmit = () => {
        window.location.href=`/pokemon/detail/${input}`
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light mb-4">
            <a className="navbar-brand" href="/">PokeDex</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
                    </li>
                </ul>
                {/* <form className="form-inline my-2 my-lg-0"> */}
                    <div className='text-center my-4'>
                        <div className='input-group'>
                            <input className='form-control' type='text' value={input} onChange={handleChange} placeholder='Buscar pokemon por nombre o id' />
                            <div className='input-group-append'>
                                <button className='btn btn-info'><i className='fa fa-search' onClick={handleSubmit}></i></button>
                            </div>
                        </div>
                    </div>
                {/* </form> */}
            </div>
        </nav>
        
    )
}
