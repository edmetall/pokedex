import React from 'react'

export default function Paginacion({irPaginaAnterior,irPaginaSiguiente}) {
    return (
        <div className='d-flex h-100'>
            <div className='align-self-start mr-auto'>
                {irPaginaAnterior && <button onClick={irPaginaAnterior} className='btn btn-outline-info text-left' > Anterior</button>}
            </div>
            <div className='align-self-end ml-auto'>
                {irPaginaSiguiente && <button onClick={irPaginaSiguiente} className='btn btn-outline-info text-right' > Siguiente</button>}
            </div>
        </div>
    )
}
