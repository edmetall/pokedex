import {useEffect, useState} from 'react'
import axios from 'axios'

export default function Details({param}) {
    const url = `https://pokeapi.co/api/v2/pokemon/${param}`
    const [datos, setDatos] = useState()
    const [abilities, setAbilities] = useState([])
    const [moves, setMoves] = useState()
    const [species, setSpecies] = useState()
    const [stats, setStats] = useState()
    const [types, setTypes] = useState()
    const [cargando, setCargando] = useState(true)

    useEffect(() => {
        const cargarDetalles = async() => {
            setCargando(true)
            try {
                const res = await axios.get(url)
                const data = res.data
                setDatos(data)
                setAbilities(data.abilities)
                setMoves(data.moves)
                setSpecies(data.species)
                setStats(data.stats)
                setTypes(data.types)
                setCargando(false)
            } catch (error) {
                console.log(error)
                setCargando(false)
            }
        }
        cargarDetalles()
    }, [])

    if (cargando) return "Cargando informacion..."
    if (!datos) return <a href='/'>No se encontro información. Clic aqui para regresar al menu principal</a>

    return (
            <div className='card '>
                <div className='text-center'>
                    <img class='card-img-top' src={datos.sprites.front_default} alt={datos.name} style={{maxHeight:'120px',maxWidth:'120px'}}></img>
                </div>
                <div className='card-body'>
                    <h5 className='text-center'>{datos.name}</h5>
                    <div className='row'>
                        <div className='col-4'>
                            <p>Height: <span className='badage'>{datos.height}</span></p>
                            <p>Weight: <span className='badage'>{datos.weight}</span></p>
                        </div>
                        <div className='col-4'>
                            <p>Order: <span className='badage'>{datos.order}</span></p>
                            <p>Base experience: <span className='badage'>{datos.base_experience}</span></p>
                        </div>
                        <div className='col-4'>
                            <p>Species: <span className='badage'>{species.name}</span></p>
                        </div>
                    </div>
                    <hr></hr>
                    <div className='row'>
                        <div className='col-2'>
                            <h5>Abilities</h5>
                            <ul className='list-group'>
                                    {
                                        abilities && abilities.map(ability=>{
                                            return <li className='list-group-item'>{ability.ability.name}</li>
                                        })
                                    }
                            </ul>
                        </div>
                        <div className='col-2'>
                            <h5>Moves</h5>
                            <ul className='list-group'>
                                    {
                                        moves && moves.map(move=>{
                                            return <li className='list-group-item'>{move.move.name}</li>
                                        })
                                    }
                            </ul>
                        </div>
                        <div className='col-2'>
                            <h5>Stats</h5>
                            <ul className='list-group'>
                                {
                                    stats && stats.map(stat=>{
                                        return <li className='list-group-item'><h6>{stat.stat.name}</h6><p>Base stat: {stat.base_stat}</p><p>Effort:{stat.effort}</p></li>
                                    })
                                }
                            </ul>
                        </div>
                        <div className='col-2'>
                            <h5>Types</h5>
                            <ul className='list-group'>
                                {
                                    types && types.map(type=>{
                                        return <li className='list-group-item'><h6>{type.type.name}</h6><p>Slot: {type.slot}</p></li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    )
}
