import {useEffect, useState} from 'react'
import axios from 'axios'

export default function Pokemon({param}) {
    const url = `https://pokeapi.co/api/v2/pokemon/${param}`
    const [datos, setDatos] = useState([])
    const [cargando, setCargando] = useState(true)

    useEffect(() => {
        const cargarDetalles = async() => {
            setCargando(true)
            try {
                const res = await axios.get(url)
                const data = res.data
                setDatos(data)
                setCargando(false)
            } catch (error) {
                console.log(error)
                setCargando(false)
            }
        }
        cargarDetalles()
    }, [])

    if (cargando) return "Cargando informacion..."

    return (
            <div className='card '>
                <div className='text-center'>
                    <img class='card-img-top' src={datos.sprites.front_default} alt={datos.name} style={{maxHeight:'120px',maxWidth:'120px'}}></img>
                </div>
                <div className='card-body'>
                    <h5 className='text-center'>{datos.name}</h5>
                    <div className='row'>
                        <div className='col-6'>
                            <p>Height: <span className='badage'>{datos.height}</span></p>
                            <p>Weight: <span className='badage'>{datos.weight}</span></p>
                        </div>
                        <div className='col-6'>
                        <p>Order: <span className='badage'>{datos.order}</span></p>
                            <p>Base experience: <span className='badage'>{datos.base_experience}</span></p>
                        </div>
                    </div>
                    <div className='text-center'>
                        <a href={`/pokemon/detail/${datos.id}`}>ver mas detalles</a>
                    </div>
                </div>
            </div>
    )
}
