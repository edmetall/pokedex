import Contenedor from "./Content/Contenedor";
import Navbar from "./Components/Navbar";

function App() {
  return (
    <div className='container'>
      <Navbar/>
      <Contenedor />
    </div>
  );
}

export default App;
