import {useParams} from 'react-router-dom'

import Details from '../Components/Details'

export default function PokemonDetail() {
    const {id} = useParams()
    return (
        <div>
            <Details param={id} /> 
        </div>
    )
}
