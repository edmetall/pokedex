import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Home from './Home'
import PokemonDetail from './PokemonDetail'

export default function Contenedor() {
    return (
        <Router>
            <Route exact path='/' component={Home}></Route>
            <Route exact path='/pokemon/detail/:id' component={PokemonDetail}></Route>
        </Router>
    )
}
